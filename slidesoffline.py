# -*- coding: utf-8 -*-
# Convierte una slides de slides.com a una slide que puede usarse sin conexion

import sys, os
import wget # install



class Contenido:
    # Objeto contenido con el enlace real del archivo y nombre del archivo
    def __init__(self, enlace, nombre_archivo):
        self.enlace = enlace
        self.nombre_archivo = nombre_archivo

        

def cargar_contenido(archivo):
    contenido = open(archivo,"r+").read()
    import re
    enlaces = re.findall(r"(https?:\/\/[\da-z\.-]+\.[a-z\.]{2,6}[\/\w \.-]*\/?)", contenido)
    print(enlaces)
    return enlaces
    


def descargar_contenido(carpeta="slidesoffline", enlaces=""):
    # Tomar solo los enlaces y nombre de archivos que hay que descargar 
    # y generar una lista objetos contenido 
    formatos = ('css', 'png', 'jpg','gif','mp4')
    contenido = []
    for enlace in enlaces:
        nombre_archivo = enlace.split('/')[-1]
        formato = nombre_archivo.split('.')[-1]
        if formato in formatos:
            contenido.append(Contenido(enlace, nombre_archivo))
    try:
        os.stat(carpeta)
    except:
        os.mkdir(carpeta)
    for c in contenido:
        wget.download(c.enlace,os.path.join(carpeta, c.nombre_archivo))
    return contenido



def reemplazar_contenido(archivo_original,contenido):
    archivo = open(archivo_original,"r+").read()
    archivo_nuevo = open( os.path.join(carpeta, archivo_original), "w")
    for c in contenido:
        archivo = archivo.replace(c.enlace,c.nombre_archivo)

    archivo_nuevo.write(archivo)
    archivo_nuevo.close()




if __name__ == '__main__':
    archivo = sys.argv[1]
    carpeta = sys.argv[2]
    enlaces = cargar_contenido(archivo)
    contenido = descargar_contenido(carpeta,enlaces)
    reemplazar_contenido(archivo,contenido)
    print("Buscando enlaces para descargar...")

